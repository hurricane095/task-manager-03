package ru.krivotulov.tm;

import ru.krivotulov.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        switch (param) {
            case (TerminalConst.CMD_HELP):
                displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                displayAbout();
                break;
            default:
                displayError(param);
                break;
        }
    }

    private static void displayHelp() {
        System.out.printf("%s - Display program version. \n", TerminalConst.CMD_VERSION);
        System.out.printf("%s - Display developer info. \n", TerminalConst.CMD_ABOUT);
        System.out.printf("%s - Display list of terminal commands. \n", TerminalConst.CMD_HELP);
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.2.1");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Aleksey Krivotulov");
        System.out.println("akrivotulov@tsconsulting.com");
        System.exit(0);
    }

    private static void displayError(String arg) {
        System.err.printf("Error! This argument `%s` not supported... \n", arg);
        System.exit(1);
    }
}
